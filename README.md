# peon.sh

![Warcraft 3 Peasant](http://classic.battle.net/war3/images/human/units/portraits/peasant.gif "Warcraft 3 Peasant")

Everyone needs it's peon.

# Why ?
Based on a [pixelastic idea](https://twitter.com/pixelastic/status/1230234084942766080)

Totally useless, therefore essential !

This `peon` function will run any command playing peasant sounds.

# How ?

You must have a vanilla Warcraft III copy and extract all peasant sounds from the `war3.mpq` to
your current user's `.peon` home directory.

This can be done easily using [MPQExtractor](https://github.com/Kanma/MPQExtractor) :
```bash
$ mkdir ~/.peon
$ MPQExtractor -e "*Peasant*.wav" -o ~/.peon /path/to/war3.mpq
$ MPQExtractor -e "*Peasant*.mp3" -o ~/.peon /path/to/war3.mpq
```

Then you can source the script and use the `peon` function :
```bash
$ source peon.sh
$ peon ls ~
```

An interactive mode is available using the `-i` flag :
```bash
$ peon -i ls
Peon will execute "ls"
Continue (y/n)?y
peon.sh  README.md
```
