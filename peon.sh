#!/usr/bin/env bash
function peon () {

  function peon_play () {
    # Check for SoX, on Debian : sudo apt install sox libsox-fmt-mp3
    if [[ -x "$(command -v play)" ]]
    then
      cmd="play -V0 -q"
    else
      # Check for PulseAudio
      if [[ -x "$(command -v paplay)" ]]
      then
        cmd="paplay"
      else
        # Fallback to ALSA Player and MPG123
        if [[ "$1" != *mp3 ]]
        then
          cmd="aplay -q"
        else
          cmd="mpg123 -q"
        fi
      fi
    fi
    $cmd "${HOME}/.peon/$1"
  }

  function peon_rand () {
    arr=("$@")
    echo "${arr[$((RANDOM % ${#arr[@]}))]}"
  }

  WHAT_SOUNDS=(PeasantWhat{1..3}.wav)
  WHAT_YES_SOUNDS=(PeasantYesAttack{1..3}.wav)
  RUN_SOUNDS=(PeasantYes{1..4}.wav)
  SUCCESS_SOUNDS=(PeasantBuildingComplete1.wav PeasantReady1.wav)
  FAILED_SOUNDS=(PeasantPissed{1..5}.wav)
  KILLED_SOUNDS=(PeasantWarcry1.wav PeasantYesAttack4.wav)
  ZOMBIES_SOUNDS=(U01Peasant15{A,B,C}.mp3)

  # Prompt before execution
  prompted=0
  if [[ "$1" == "-i" ]]
  then
    peon_play "$(peon_rand "${WHAT_SOUNDS[@]}")"
    shift
    echo "Peon will execute \"$*\""
    read -r -p "Continue (y/n)?" c
    case "$c" in
      y|Y )
        peon_play "$(peon_rand "${WHAT_YES_SOUNDS[@]}")"
        prompted=1
        ;;
      * )   return;;
    esac
  fi

  # Run command
  if [[ $prompted -ne 1 ]]
  then
    peon_play "$(peon_rand "${RUN_SOUNDS[@]}")"
  fi
  "$@"
  rc=$?
  case $rc in
    0)    sound="$(peon_rand "${SUCCESS_SOUNDS[@]}")";; # Command succeeded
    126)  sound="PeasantCannotBuildThere1.wav";;        # Can't execute
    127)  sound="PeasantWhat4.wav";;                    # Command not found
    137)  sound="$(peon_rand "${KILLED_SOUNDS[@]}")";;  # Processed killed
    *)    sound="$(peon_rand "${FAILED_SOUNDS[@]}")";;  # All other errors
  esac
  peon_play "$sound"

  # Bonus, detect zombies processes
  if [[ $(ps -eo state|awk '$1=="Z"'|wc -l) -ne 0 ]]
  then
    peon_play "$(peon_rand "${ZOMBIES_SOUNDS[@]}")"
  fi

  return $rc
}
